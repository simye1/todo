'use strict';
const { v4: uuidv4 } = require('uuid');

class TodoService {
  constructor(todoDataAdapter, todoEventEmitterAdapter) {
    this.todoDataAdapter = todoDataAdapter;
    this.todoEventEmitterAdapter = todoEventEmitterAdapter;
  }

  async listTodo() {
    try {
      return await this.todoDataAdapter.listTodos();
    } catch (e) {
      console.error(`error wile listing todo title`, e);
      throw e;
    }
  }

  async getTodo(id) {
    try {
      return await this.todoDataAdapter.getTodo(id);
    } catch (e) {
      console.error(`error wile getting todo id ${id}`, e);
      throw e;
    }
  }

  async createTodo(todo) {
    todo.id = uuidv4();
    try {
      const createdTodo = await this.todoDataAdapter.createTodo(todo);
      // await this.todoEventEmitterAdapter.emitToDoCreatedEvent(createdTodo);
      return createdTodo;
    } catch (e) {
      console.error(`error wile creating todo title ${todo.title}`, e);
      throw e;
    }
  }

  async updateTodo(todo) {
    try {
      return await this.todoDataAdapter.updateTodo(todo);
    } catch (e) {
      console.error(`error wile updating todo title ${todo.title}`, e);
      throw e;
    }
  }

  async markDone(id) {
    try {
      const toBeDoneTodo = await this.todoDataAdapter.getTodo(id);
      toBeDoneTodo.status = 'DONE';
      await todoDataAdapter.updateTodo(toBeDoneTodo);
      return toBeDoneTodo;
    } catch (e) {
      console.error(`error wile marking todo done id ${id}`, e);
      throw e;
    }
  }

  async deleteTodo(id) {
    try {
      await this.todoDataAdapter.deleteTodo(id);
    } catch (e) {
      console.error(`error wile deleting todo id ${id}`, e);
      throw e;
    }
  }

}

module.exports = TodoService