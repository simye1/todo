module.exports = function initRoutes(router, controller) {
    router.get('/', controller.listTodos.bind(controller));
    router.get('/:todoId', controller.getTodo.bind(controller));
    router.post('/', controller.createTodo.bind(controller));
    router.put('/:todoId/markdone', controller.markDone.bind(controller))

    return router;
}