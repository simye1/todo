const { PubSub } = require('@google-cloud/pubsub');
const Todo = require('../model/todo');

const pubSub = new PubSub();
const subscription = pubSub.subscription('todo_updated');

module.exports = async function init(todoService) {
  subscription.on(`message`, async (message) => {
    console.info(`Received message ${message.id}:`);
  try {
    const todoTransport = JSON.parse(message.data.toString());
    const todo = new Todo(todoTransport.id, todoTransport.title, todoTransport.description, todoTransport.status);
    const updatedTodo = await todoService.createTodo(todo);
    console.info(`Todo id ${updatedTodo.id} got updated`);
    message.ack();
  } catch (e) {
    console.error('Error while processing message from pub/sub', e);
  }
  });
}