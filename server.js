'use strict';
const express = require('express');
const app = express();
const router = require('express').Router();
const scheduler = require('./src/scheduler/todoScheduler');

app.use(express.json());
app.use(express.urlencoded({extended: false}));

const todoEvent = require('./src/event_handler/todoEventHandler');
const todoRoutersV1 = require('./src/api/v1/routes/todo');
const TodoControllerV1 = require('./src/api/v1/controller/todoController');
const TodoService = require('./src/service/todoService');
const TodoDataAdapter = require('./src/adapter/todoDatasourceAdapter');
const TodoEventEmitterAdapter = require('./src/adapter/todoEventPubsubAdapter');

const todoDataAdapter = new TodoDataAdapter();
const eventEmiter = new TodoEventEmitterAdapter();
const todoService = new TodoService(todoDataAdapter, eventEmiter);
const todoController = new TodoControllerV1(todoService);

scheduler(todoService);
todoEvent(todoService);
app.use('/v1/todos', todoRoutersV1(router, todoController));

const server = app.listen(3000, () => console.log('Running…'));

process.on('SIGTERM', shutDown);
process.on('SIGINT', shutDown);

let connections = [];

server.on('connection', connection => {
  connections.push(connection);
  connection.on('close', () => connections = connections.filter(curr => curr !== connection));
});

function shutDown() {
  console.log('Received kill signal, shutting down gracefully');
  server.close(() => {
    console.log('Closed out remaining connections');
    process.exit(0);
  });

  setTimeout(() => {
    console.error('Could not close connections in time, forcefully shutting down');
    process.exit(1);
  }, 10000);

  connections.forEach(curr => curr.end());
  setTimeout(() => connections.forEach(curr => curr.destroy()), 5000);
}